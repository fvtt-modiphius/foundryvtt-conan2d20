# v2.3.0

## Enhancement
- [#395] Allow modification of default Wounds/Traumas for NPC to allow implementation of Immense monster quality

	* The NPC sheet will now only set the default maximum Wound/Trauma values when the NPC's type has been changed to allow for customising this value

- [#438] Minor layout tweaks to Character Upgrade interface
- [#439] Add option to enable/disable adding all core Action items to a character when it is created (enabled by default)
- [#440] Minor tweaks to Talent chat data format
- [#441] Display Range/Reach of weapon attack on skill test chat card
- [#444] Add new icon to distinguish between melee/ranged attacks in Action list
- [#445] Character sheet keyboard input of vigor/resolve/fatigue/despair enabled by default to avoid confusion for new users of the system
- [#446] Performance improvements to loading of the Character Creator and Character Upgrade interfaces
- [#447] Add a loading progress bar to the Character Creator and Character Upgrade tools
- [#450] Implement journal CSS theme
- [#451] Update existing adventure journals to take advantage of new journal theme
- [#453] Add new compendium of player facing transport items from the core rulebook

## Bugfix
- [#436] Compendium item link buttons not working on character sheet, character creator or level up tool
- [#437] Loading of resources stuck at 97% in Firefox
- [#442] NPCs and Pre-generated characters missing from To Race the Thunder adventure
- [#443] Momentum interface overlaps the right-click toolbar menu
- [#448] Availability choices should include a Simple (D0) option

## Core
- [#449] Update sass packages to remove deprecation warning when building
- [#452] Remove the page number field for sources to keep things simple

---

# v2.2.7

## Enhancement
- [#435] Change the z-index of the Momentum Tracker interface

---

# v2.2.6

## Enhancement
- [#411] Implement control on skill rolls dialog to allow PC/NPC to skip the automated difficulty increase due to wound penalties

## Bugfix
- [#422] Defeated not applying Icon

## Chore
- [#424] Remove "allowBugReporter" from system.json
- [#425] Remove deprecated use of Sass lighten/darken in CSS definitions
- [#426] Update node and packages used by build process to latest LTS
- [#427] Remove use of deprecated Sass @import rules from CSS
- [#428] Math.clamped is deprecated in favor of Math.clamp
- [#429] Stop accessing the global "DiceTerm" which is now namespaced under foundry.dice.terms.DiceTerm
- [#430] StatusEffectConfig#label has been deprecated in favor of StatusEffectConfig#name
- [#431] StatusEffectConfig#icon has been deprecated in favor of StatusEffectConfig#img
- [#432] globalThis.getProperty must now be accessed via foundry.utils.getProperty
- [#434] globalThis.setProperty must now be accessed via foundry.utils.setProperty

*NOTE: As of this release the Conan2d20 system only supports FoundryVTT v12 and above*

---

# v2.2.5

## Bugfix
- [#420] Error thrown when adding Actors to combat tracker

---

# v2.2.4

## Bugfix
- [#418] NPC target armor rating not reducing damage when applied

---

# v2.2.3

## Bugfix
- [#415] Character advancement using incorrect costs for talent purchases
- [#416] Phoenix Effect Rolls deal no damage

---

# v2.2.2

## Enhancement
- [#414] Add modified Crush Your Enemies font which includes more characters

## Bugfix
- [#412] Issue with character sheet on screens with height res equal or smaller than 900

## Chore
- [#413] globalThis.randomID must now be accessed via foundry.utils.randomID

---

# v2.2.1

## Bugfix
- [#409] Spend/Bank Momentum tool fails with error in the console

## Chore
- [#401] Merge Polish i18n updates from Crowdin

---

# v2.2.0

## Enhancement
- [#364] Add documentation for character sheet controls
- [#396] Allow modification of Health values via direct number edit for Tablet/Phone users

	* Added a new system setting to enable this behaviour.

- [#398] Add some indication about whether the actor roll being performed is considered a GM or Player roll

	* New toggle added which is visible to GMs which means they can see whether the actor is player owned (and will therefore use `momentum`), or not. The GM can toggle this as needed.

## Bugfix
- [#399] Cannot drag/drop stowage items onto watercraft sheet
- [#400] Skill Roll macro not working
- [#407] Fortune talents not accessible in Character Upgrade tool

## Chore
- [#390] Remove deprecated Transportation items
- [#401] Ensure the system has no compatibility issues in Foundry V12 Development Releases
- [#402] Remove deprecated gridDistance and gridUnits options in system.json
- [#403] globalThis.duplicate must now be accessed via foundry.utils.duplicate
- [#404] globalThis.mergeObject must now be accessed via foundry.utils.mergeObject
- [#405] {{select}} handlebars helper is deprecated
- [#406] The async option for Roll#evaluate has been removed

---

# v2.1.2

## Bugfix
- [#392] Skill Rolls for player owned mounts showing skills dropdown instead of expertise
- [#393] Incorrect template paths cause Character Creator failure to launch

---

# v2.1.1

## Enhancement
- [#144] Display Combat Dice phoenix in texts

	* Text enrichment now detects the `§`, `CD` and `DC` strings and replaces them with the Combat Dice phoenix icon.  This means you should be able to just copy/paste text from the PDFs and the symbol will be displayed correctly.

## Bugfix
- [#391] System specific status effects missing after update

---

# v2.1.0

## Bugfix
- [#361] Players can see blind rolls results
- [#381] Enchantment qualities not displaying on item sheet
- [#384] Apply Damage menu option shouldn't be shown on non-damage chat cards
- [#385] Damage roll results chat message shows incorrect value when weapon has Improvised quality
- [#387] Ensure we don't allow duplicate entries to be selected in the character creator if multiple form submissions occur

## Enhancement
- [#352] Add support for NPC Inhuman X abilities, which add additional successes to a roll using that ability
- [#353] Keep a viewable log of all spent XP on a character
- [#383] Allow Bonus section of Skill Roller to be minimised
- [#389] Show tooltips on Actor Capabilty/Trait/Quality tags
- [#344] Make Transportation Items full class Actors

	* There are now new Mount, Transport and Watercraft actors, complete with inventories for stowed items.
	* The Stow Item feature of the character sheet has now been turned into a Send Item tool which allows you to send an item to the inventory of any Actor you have the required permissions over, this can be used to stow items in the new actor types.
	* Any mount Transortation items that a character owns will be migrated to a Mount Actor, and Items stowed in it will be transferred to this new Actor.
	* Watercraft actors can own and use NPC Attack items, although as the actual attack rolls for these weapons are performed by the individual using or commanding the vessel, only the weapon's damage roll can be performed from the sheet.
	* Transportation items will now no longer be displayed on a character sheets inventory tab

## Deprecation Warning
Now that Transportation items are full class actors, the old Transportation Items will be removed from the system in version v2.2.0.  It is recommended that you recreate any necessary items before this time.

---

# v2.0.13

## Bugfix
- [#377] Fail more gracefully if a talent requirement cannot be found
- [#378] Character Creator gets stuck in a message loop when more than one player connected when it is in use.

---

# v2.0.12

## Bugfix
- [#371] Skull icon path broken on damage report chat message
- [#372] During conversion from Weapon to NPC Attack, the original item's damage rating is altered
- [#373] Skill test enricher regex too greedy and captures more than it's supposed too

## Enhancement
- [#374] Display the system release notes the first time a world is loaded in a new version of the system

---

# v2.0.11

## Bugfix
- [#368] Unable to perform skill checks or expand items on character sheets (redux)

---

# v2.0.10

## Bugfix
- [#368] Unable to perform skill checks or expand items on character sheets

---

# v2.0.9

## Bugfix
- [#367] Dragging background compendium item onto character sheet should not attach item to character

## Chore
- Merged new Spanish language updates from Crowdin

---

# v2.0.8

## Bugfix
- [#363] Revisited issue as the cost of Talent/Attribute and Skill upgrades still keeps accumulating if you select and then cancel an upgrade

---

# v2.0.7

## Bugfix
- [#360] Skill Test enrichments fail to match on non-ascii characters
- [#362] Ensure the English version of Skill/Expertise names can always be used in the skill roll chat enrichments
- [#363] Incorrect cost calculation when upgrading Attributes

---

# v2.0.6

## Bugfix
- [#357] Compendium item selectors need unique IDs to ensure they show the correct selection

## Chore
- Merged new Polish language updates from Crowdin

---

# v2.0.5

## Bugfix
- [#356] Fix data issue with old armor item types not allowing characters sheets to open

## Enhancement
- [#355] Replace mentions of `d6` in tooltips with `[CD]` for consistency.
- [#355] Don't display `X` in qualities if there is a value set, use the value instead

## Chore
- Merged new Polish language updates from Crowdin

---

# v2.0.4

## Bugfix
- [#354] Fixed rare case of being unable to open character sheet due to old/bad data in armor item.

---

# v2.0.3

## Bugfix
- [#348] Armor qualities tags not displayed correctly on character sheet or in chat
- [#349] NPC name input mis-alignment issue
- [#351] Error in character creator when educations requiring random archetype skills are rolled/chosen

---

# v2.0.2

## Bugfix
- Unable to select Castes on Character Sheet

---

# v2.0.1

## Bugfix
- Bumped version number in system.json

---

# v2.0.0

## Bugfix
- Previous schema migration didn't correctly delete fields that are no longer used
- Removed the system specific default token settings as this can be configured in the core Foundry settings now
- Broken weapons no longer show up in list of Attacks on character sheet
- Removed unused Weapon Group variable from Weapons and NPC Attacks
- "Story" missing from Character Biography section
- Tidied up the "readonly" character sheet shown when players have limited view of a character
- Set the default Attribute in the Skill Roller interface to the correct Attribute for NPC Melee (Agility), Ranged (Coordination) and Threaten (Personality) attack rolls.
- Don't show Fortune spend confirmation for D0 skill rolls
- Added missing translation stings from Skill and Damage roller chat messages

## Enhancement
- Added support for Penetration to Apply Damage
- Apply Damage tool now creates a chat message summary of damage once applied
- Added ability to toggle on/off weapon qualities in the Apply Damage tool for situations where they should not be applied
- Apply Damage tool now supports the following weapon qualities, and adjust damage/conditions accordingly: Blinding, Cavalry X, Fearsome X, Grappling, Improvised, Incendiary X, Intense, Knockdown, Non-lethal, Persistent X, Piercing X, Spread X, Stun, Unforgiving X, Vicious X
- Added the ability to launch the Apply Damage interface directly from a Damage Roll chat card (right-click menu item). This will auto-populate all values and selected tokens as targets (GM only)
- Added Character Upgrade interface that lets you spend XP to improve Attributes, Skills, and also to buy Talents that the character meets the requirements for
- Added Character Creation interface
- Added macro to launch Character Creator
- Added button at bottom of Actor tab to launch Character Creator
- Added Archetype Item type and hooked selector up to character sheet
- Added Caste Item type and hooked selector up to character sheet
- Added Education Item type and hooked selector up to character sheet
- Added Language Item type and hooked selector up to character sheet
- Added Homeland Item type and hooked selector up to character sheet
- Added Story Item type and hooked selector up to character sheet
- Added Nature Item type and hooked selector up to character sheet
- Added War Story Item type and hooked selector up to character sheet
- Added custom Layer Control menu containing some of the GM Tools already available via macros (Initialize Game, New Scene and Request Skill Test to begin with)
- Provide "Best Effort" data migration from old text background fields to new Items (only Items from Core rulebook)
- Added Character creation roll tables using new Items
- Beauty pass on all Item sheets
- Added support for Crew weapon size and related Crew Count value
- Item descriptions now use enriched descriptions in the editor
- Added i18n keys for Actor and Item types
- Added Grappled status
- Added ability to request Skill rolls from players via chat and journal links
- Added interface for easily requesting Skill Tests, as well as the ability to create the relevant link to add to journals
- Support customizable compendium of default items to be added to new characters created by the Character Creator
- NPC Mobs now also have their member's token image path stored on the Mob/Squad leader at: `actor.system.grouping.memberToken`
- Provide ability to specify which books Sources are allowed to be used in the Character Creator and Upgrade tools
- Polish language support added

## Behaviour Change
- On the player character sheet, you must now hold the Ctrl key down to adjust Vigor/Fatigue/Resolve/Despair values by mouse click/wheel.  Additionally, you must now hold the Alt key down and left-click to reset a Vigor/Fatigue/Resolve/Despair value

## Chore
- Improve data migration tooling
- Remove dead fields from character template
- Build data packs from json objects to make maintenance easier
- Build language files from flattened YAML files to make maintenance and translation work easier
- Import legacy FR translation into base system
- Setup and link Crowdin project to assist with translation work

---

# v1.9.4

## Bugfix
- Reverted tooltips to default browser behaviour as Foundry tooltips are too "in your face"
- Fix styling on Character Biography and Notes tabs

---

# v1.9.3

## Bugfix
- Expanded attack details Damage button on NPC sheet doesn't work
- Item context menu not working for normal users
- Stop scroll locations changing on character sheet re-render
- Condition icons not showing in v11

---

# v1.9.2

## Bugfix
- Multiple token copies of an Actor set as defeated instead of just the actually defeated one
- Combat tracker won't begin combat in v11

---

# v1.9.1

## Bugfix
- Spanish language translation config entry missing from system.json
- Duplicate copy of Increment Doom macro
- Missing copy of Increment Momentum macro
- Damage Application tool incorrectly adding Vigor/Resolve when Armor/Courage is greater than damage applied
- Slaver NPC in Pit of Kutallu owns bogus "weapon" Item types
- Only allow Apply Damage shortcut to appear on actual Token sheets, or Actors with "Link Actor Data" enabled in the token prototype.
- NPC Wounds applying twice
- Missing map scene thumbnails

## Chores
- Validate with Foundry v11

---

# v1.9.0

## Bugfix
- Incorrect path to splash screen image in system.json
- Don't display Hit Location for mental damage rerolls
- Links not being displayed correctly in Character and NPC Biography and Notes sections
- Weapon size information missing from NPC attack items
- Bonus Successes not being included when calculating skill check success/failure
- Talent properties/qualities tooltips not displaying correctly
- "Use Kit" button fixed. On a Kit item with spare uses it will ask for the amount of uses to spend on extra dice and trigger a skill roll for the skill linked to the item.  The the remaining uses on the item is updated and the skill roll is prepopulated with the correct number of bonus d20s

## Enhancement
- When creating an item, set its icon to the system default for that type
- Add the Quickstart Rules and "To Race the Thunder" adventure to the system (many thanks to theczarek for the conversion work)
- Give the ability to override an Actor-based skill roll Complication range
- Reduce the amount of icon clutter on character sheets by moving Edit and Delete to a right-click context menu for all item types
- Add missing tooltips in the Belongings list
- Sort NPC attacks alphabetically and group into Weapon and Display attacks for clarity
- Completely rework the layout of the NPC sheet
- Display individual custom NPC traits, rather than just "Custom"
- Add Attack, Damage and Soak icons to Attack Actions on sheets to make it more
efficient to trigger these rolls
- Character and NPC Biographies and Notes now using the ProseMirror editor in their sheets
- Allow dragging and dropping items to/from the NPC character sheet
- Custom "Game Paused" element
- Better contrast and larger font size to aid readability on Momentum/Doom counter
- Give ability to track Mob/Squad members' health and wounds on the parent NPC sheet
- Allow setting of Mob/Squad member by dragging and dropping a suitable Minion NPC onto the Mob/Squad tab of a the parent NPC
- Provide information on the skill roll chat message about the number of assist dice rolled, as well as the TN/Expertise numbers used to calculate these successes
- Add nicer default NPC/Character token images rather than the usual Foundry cowled face image
- Add "venom" weapon quality
- Added tooltips for Skills
- Added macros for Incrementing/Decrementing Doom and Momentum
- Added Weapon Group to npcattack items and created a new "Natural" weapon group that can be used
- Dragging a melee or ranged npcattack item to a player character sheet automatically converts it into a weapon item and removed any built in NPC damage bonsuses so it can be used by the player character
- Dragging a player character weapon to an NPC sheet automatically converts it into an npcattack item and applies any relevant damage bonsuses so it can be used by the NPC
- Add mobMembersAlive value/max data pair to actor dynamically to help with tracking mobs (Bar Brawl, etc.)
- Create interface for applying damage to Actors, handling Mobs/Squads and individual Characters and NPCs.
- Add "The Pit of Kutallu" adventure to the system (many thanks to theczarek for the conversion work)
- Include a compendium of scenes comprising versions of The Lands of the Hyborian Age maps (many thanks to Glynn Seal, Joseph Hepler and David Thomas for allowing us to use them)
- Update system documentation to include new Apply Damage tool, and NPC Sheet and Mob/Squad usage
- Custom Combat Tracker

---

# v1.8.1

## Bugfix
- "New Scene" and "Initialize Game" macros fail if there is a player with no character.

---

# v1.8.0

## Bugfix
- Brittle armor quality typo fixed
- Journal titles unreadable when viewed with Monk's Enhanced Journal
- Points for effects not being added to custom dice rolls in chat when rolling via a `/r {x}dp` command
- Roll Modes not being applied properly to chat messages
- Shield Soak button appearing on Item card even if the item doesn't have the Shield X quality
- TextArea spacing issue causing padding to be added to Spell/Enchantment Item text
- Token effect icons broken in Firefox 104.0.1

## Enhancement
- Add a dialog for rolling basic Combat Dice
- Add ability to trigger shield soaks from the item details panel on character sheet or chat message
- Added "Basic Skill Roll" macro
- Added "Combat Dice Roll" macro
- Added "Cover Soak Dice Roll" macro
- Added "Damage Roll" macro
- Added "Initialize Game" macro
- Added "Morale Soak Dice Roll" macro
- Added "New Scene" macro
- Added "Skill Roll" macro
- Added "Soak Dice Roll" macro
- Allow Items to be dragged to the Hotbar
- Expanded/Collapsed tabs on the character sheet remember their state during a session
- Fixed Fortune "1" rolls should be shown at the beginning of dice results
- Health schema changes for consistency [internal]
- Improved Doom & Momentum tracking
- Input field color consistency improved
- Make encumbrance setting for Items free text rather than selector
- NPC Sheets now have Max Wounds and Max Traumas fields to aid tracking damage
- Started adding some basic system documentation
- Tidy and enhance the Damage Roll dialog
- Tidy and enhance the Skill Roll dialog
- Tidy and enhance the Soak Roll dialog
- Tidy and improve the Momentum Banking dialog

---

# v1.7.1

## Bugfix
- Fixed unable to edit Display action on character sheet
- Fixed item chat card overlapping text
- Fixed PCs unable to attack with Display Action
- Swap to SVG images from WEBP bitmaps for better dice images

---

# v1.7.0

## Enhancement
- V10 Foundry Support!
- Character Sheet reorganization
- Added more XP / Fortune values
- Skill Roll Cards Overhauled
- Added Conditions for tracking Blocking and Prone
- New dice images for chat cards
- General usability fixups
- Allow encumbrance values on miscellaneous items
- Improved accessibility on NPC skills

## Bugfix
- Fixed mob rolls
- Added stowage items to migration scripts
- Added World compendium to migration scripts
- Fixed issue with item id's in schema update to allow miscellaneous items to be stowed
- Removed use of deprecated entity field on package definitions
- Fixed incorrect kit types and costs for reloads
- Fixed incorrect reroll of hit location
- Fixed npcattack caht button to correctly use actor when calculating rolls for unlinked actors

---

# v1.6.1

## Bugfix
- Fix breakage in stowage for unmigrated actors

---

# v1.6.0

## Enhancement
- Major refactor of transportation encumbrance tracking and item stowage
- Consumable items can now be increased/decreased with button presses
- Character sheet sections are now collapsable

## Bugfix
- Fixed character profile image ui overlap
- Added missing summary labels
- Fixed equipped weapon encumbrance miscalculation
- Condition labels now use i18n text
- Fixed display of shield soak in armor section of character sheet
- Cleanup of icon and font sizes across character sheet
- Some cosmetic cleanup of chat cards
- Removed deprecated deleteOwnedItem calls causing npc corruption
- Fixed value field name for traumas

## Chores
- Fixup to CI jobs

---

# v1.5.0

## Enhancement
- Added prefill for difficulty and attribute selection on NPC skill rolls
- Added the ability for players to arbitrarily alter and track their Maximum health
- Added the ability for players to alter their current health with simple click, right-click, control+click
- Added sorting for Weapon and Display attacks on the Action tab

## Bugfix
- Only equipped weapons now have actions added to the actions tab.
- Fixed a subtle bug in damage re-rolls. Re-rolls should now include the static dice from the original roll.
- Fixed a bug in damage re-roll calculation. Reroll damage totals should now be inclusive of any static damage from original roll.
- Fixed a breakage in our schema migration code. Migrations should now again be possible when necessary.
- Fixed a bug that prevented petty enchantments from being posted to chat.
- Fixed a bug that prevented lotus pollen from altering a trait
- Refactor of character details and inclusion of text input for character 'Natures'
- Removed the unecessary attribute roll button.
- Various style and compendium fixes

## Chores
- Refactored migration 1 to include safety check for current state

---

# v1.4.0

## Enhancement
- Refactor organization of talent tab into ordering by Tree
- Added Maximum Vigor and Resolve tracking to NPC sheet

## Bugfix
- Fixed deprecated function call to resolve broken NPC attacks
- Fixed vigor and resolve coloring for readability
- Fixed NPC wound tracking bug caused by conditional template compilation
- Fixed NPC Token Health always bar set to value of 7

## Chores
- Minor templating and style cleanup


---

# v1.3.0

## Enhancement
- Added simple 'Harm' tracking for 'Toughened' and 'Nemesis' NPCs
- Added arbitrary bonus d20 handling to skill rolls
- Added Calculated arbitrary momentum bonus to skill rolls
- Added support for Foundry V9 release

## Bugfix
- Fixed several typos typos in compendiums (Thanks to our new contributor! @Muttley1)
- Fixed flow Bug in PC Harm tracker (another wonderful contribution by @Muttley1)
- Fixed Armor Soak values for Brindaine Vest
- Fixed character sheet health data corruption

---

# v1.2.0

## Enhancement
- Added support for assist rolls
- Added chat message for momentum/doom banking/spends
- Added Max health to health tracker
- Added default Autolink NPC tokens to npc sheets
- Added Attribute rolls
- Added Mob count and tracking to NPC sheet
- Added upkeep cost tracking to inventory
- Added NPC dice rolls into Mob Tracker

## Bugfix
- Fixed ViciousX Title Card
- Fixed misrolled fortune conversion for NPCS
- Fixed armor coverage and condition tracking breakage
- Fixed Regression in NPC creation
- Fixed Encumbrance Tracking fix 0.8.X
- Fixed talent addition bug in NPC sheet
- Fixed poison/stagger application bug
- Fixed 0.8 relaod spends
- Fixed Minion rolls on skill checks
- Fixed item sheet for 0.8.x
- Fixed chatcard roll buttons
- Fixed DSN Integration for 0.8 changes
- Fixed token condition handling
- Fixed 0.8 npc sheets
- Fixed 0.8 armor sidebar
- Fixed 0.8 health sidebar
- Fixed 0.8 Character sheet

## Chore
- Update Node dependencies where sane.
- Update ESlint to match new JS code.
- Migration from typescript to javascript.

---

# v1.1.0

## Enhancement
- Addes Compendiums for Free Core Book Content
- Added intimidating quality name to the localized list.
- Added new Item Qualities
- Added Easy difficulty (0) to possible choices.
- Updated all compendiums to include icons and more/better data.
- Added New icons for actions & displays & talents

## Bugfix
- Various combat roll fixes #200, #197, #199
- Updated compendiums to use qualities (displays, weapons and armors)
- Missing label for Regal weapon quality. Forgot the L at the end of the 4 letter code.
- Resolve "No option for D0 Simple tests on skills"
- Replaced erroneous Easy with Simple for D0 checks
- Resolve NPC attacks can go beyond 9 CD
- Fix #201 -- added 11-20 dice rows to CONFIG.damageDice
- Cleanup of packs Added Actions Compendium
- Allow passing of both doom and momentum spends on a single roll
- (fix): Swap melee weapon reach with text input
- (fix): Migrate Talent tree to text input field

## Chore
- Commit squashes lost data.
- Replace Author with Author(s) in system.json
- Use build artifacts from release branch temporarily

---

# v1.0.0

## Enhancement
- Full rewrite in typescript
- Initial version of the NPC sheet redesign, still bugged by base.ts getData() labelling.
- Initial Item Posting
- Initial talents tab and sheet logic
- More decomposition of data model
- Replaces UI styling for all char-sheet tabs
- Restyle of inventory tab, update equip toggle color
- Revamps wound tracking to allow for treated wounds
- Added missing and incorrect content fields for armor, weapons, talents, and actions
- Added form to configure tiles and drawings for morale and cover soak rolls
- Added form to configure tiles and drawings for morale and cover soak rolls
- Added development notes to contribution doc
- Added readonly sheets, fixes languages box
- Added miscellaneous description-only item type and inventory handling
- Added missing titles and translations
- Added more information on commit signing
- Added selectable NPC attribute and calc for npc skill rolls.
- Added styling to tags on chat cards.
- Added Prefill dialog for rolls
- Swaps license to MPL v2. Explicitly limiting the use of trademarked materials in derivative works
- Added system manifest into release artifacts
- Added DSN Integration and CombatDie class
- Added check to make sure only GMs edit Counters on socket calls
- Added socket support for counters, added setting for users to edit
- Added Initial Momentum/Doom counter
- Added Noble Warrior background data
- Added the repo address in the manifest to make the system easily downloadable.
- Added skills on the left hand pane of actor sheet, issues with the CSS GRID
- Added data to all compendiums linked in the system, not all data types have been added to their compendiums (for example: not all background types ares in background.db). Fixed a few forgotten fields in template.json. Remove 2 useless files.
- Added the logic to prevent vigor and resolved to go below 0
- Added Celtic font, working on structuring the skills tab
- Added #references from .gitignore
- Added momentum spend context button
- Added gold pouch icon and and title card
- Added transporation items and encumbrance handling
- Added title class data for health/armor items
- Added title class data for conditions and qualities. Minor style tweaks
- Added gold handling to belongings tab, fix missing titles
- Added initial chat cards and some basic chat card styling
- Added sorcery and enchantments
- Added encumbrance calculation and trackin on inventory page
- Added despair/fatigue tracking several npc bug fixes
- Added npc categories and roll handling
- Added action handling and reorg sheet html
- Added attack inventory to NPC card
- Added unique chatcards for different item types, links post to header icon
- Added armor/shield tracker to sidebar
- Added armor calculation logic from equipped armors
- Added kits/resource spends/inventory expansion/bugfixes
- Added damage rolls and action generation
- Added reroll and fortune handling logic to skill tests
- Added initial skillroll logic and fortune handling
- Added weapon sheet template and logic
- Added equality check handlebars helper
- Added token settings registration
- Added defaultTokenSettings setting registration
- Added config mechanism for handlebars helpers and actor updates
- Added Talent Sheet
- Armor tracker style update
- Completes first pass of npc sheet.
- First step to changing item layout, convert equipment to armor
- Denormalize armor between npc and character actors
- Wire condition tracking through to token
- Wires momentum and doom pools to dice rolls

## Bugfix
- Corrects trait-handler behavior for all call sites
- Fixes kit and action selectors for mixed value dropdown
- Fixes CI runs against master
- Fixes rendering of enchantment traits
- Removes unused sheet from template paths
- Shift manifest path to raw master file
- Various fixes to action cards and chat cards
- Correct release url typo
- Remove unnecessary prep job and caching
- Disable linting when triggered by a tag
- Fixes trait selector vars in html
- Add 2 missings langages, change a typo in foundryconfig.example to avoid mistakes
- Major formatting issues resolved across most .ts files
- remove console.logs from trait selector, pull new types
- Changes availability to match difficulty rolls
- Cleanup and corrects attributes, expertise, and soak for npc sheet
- Cleanup and fixes for item dragging
- Cleanup of all data handling to a true state of template.json
- Cleanup unused templates, refactor templates
- Corrects object merge for PC actions
- Corrects all data inputs to appropriate types
- Corrects nested qualities value
- Corrects actor sheet data update bug
- Fix armor quality tagging
- Fix NPC item addition, armor quality calculation
- Fix sorcery styling and broken add new button
- Fix item sheet tabs
- Fixed a few changes from the original work procces. Gulp -> Webpack and guidance on the push process.
- Fixed the Input Height of the h1.char-name to fully cover the height (highest => "L" and lowest => "q") of CrushYourEnemies font.
- Fixed the Character Actor Sheet: Adjusted layout, Added the required fields on the Background tab, fixed the spells tab. Widened the default sheet size by 100px. Reduced font-size. Added some strings to the language file.
- Fixes inability to add talents with button
- Fixes spell sheet modification error.
- Fixes breakage in inventory damage roll reload spend
- Fixes failing dropdown for damage dice
- Move doom tracker to bottom of screen over macro bar

## Chore
- Create CONTRIBUTING.md
- Create CODE_OF_CONDUCT.md
- Migrate from gulp to webpack
- Correcting some informations and moving pertinent details from README to CONTRIBUTING. Clean up the packs. Added templates for issues and merge requests.
- Configures eslint and prettier for sass, js
- Changes gulpfile data dir updates readme with examples
- Alter build so webpack can run with the package.json parameters.
- Precommit fixup
- Adds notes on pre-commit and DCO to the contributing docs
- Adds pre-commit checks into CI
- Adds lints to all files for a PR branch
- Formatting and linting across remaining repo files
- Sets release stages to only trigger on `tags`
- Update linting configs, add pre-commit config
- Finalize release job
- build with latest
- Update .gitlab-ci.yml
- Update foundryconfig.example.json to provide examples of datapaths under Linux and Windows.

## Wires
- Wires momentum and doom pools to dice rolls
