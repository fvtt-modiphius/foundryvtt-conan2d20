/**
 ***************************************************************
 * This macro can be used to launch the Conan Character Creator
 **************************************************************/

new conan.apps.CharacterCreator().render(true);
