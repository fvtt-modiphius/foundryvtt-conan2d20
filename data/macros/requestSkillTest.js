/**
 ***************************************************************
 * This macro can be used to open the Request Skill Test User
 * Interface
 *
 * NOTE: Only users with the Game Master user role can run this
 * macro.
 **************************************************************/

game.conan2d20.macros.requestSkillTest();
