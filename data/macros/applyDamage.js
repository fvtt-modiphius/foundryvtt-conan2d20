/**
 ***************************************************************
 * This macro can be used to apply damage to one or more Actors
 * depending on whether the user is a GM or Player.
 *
 * If the user running the script is a Player, the Apply Damage
 * tool will allow you to apply damage to your currently
 * selected character only.
 *
 * If the user is a GM, it will apply damage to all selected
 * tokens.
 **************************************************************/

game.conan2d20.macros.applyDamage();
